#pragma once
#include <cpprest/json.h>
#include <CascadeLemmatizer.h>

namespace lemmatization {
class label {
public:
    int end;
    int endToken;
    std::string fieldName;
    std::string name;
    double score;
    std::string serviceName;
    int start;
    int startToken;
    std::string value;

    label(web::json::value json_label); 

    web::json::value jsonize();
};

class doc {
public:
    doc(web::json::value json) {
        initialize(json);
    }
    std::vector<label> makeNewLabels(CascadeLemmatizer& lemmatizer);
private:
    void initialize(web::json::value json);
    void initLabels(web::json::value json);
    void initLemmas();
    std::vector<label> labels;
    std::map<int, std::pair<std::string, std::string>> lemmas;
};

void lemmatizeJSON(web::json::value& json);
} // lemmatization
