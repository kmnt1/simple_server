﻿#include <iostream>
#include <map>
#include <set>
#include <string>

#include "unicode/unistr.h"
#include "unicode/ustream.h"
#include "json_lemmatization.hpp"

#include <cpprest/http_listener.h>
using namespace web;
using namespace web::http;
using namespace web::http::experimental::listener;

void handle_request(
    http_request request,
    std::function<void(json::value const&, json::value&)> action)
{
    auto answer = json::value::object();

    request
        .extract_json(true)
        .then([&answer, &action](pplx::task<json::value> task) {
        try
        {
            auto const& jvalue = task.get();

            if (!jvalue.is_null())
            {
                action(jvalue, answer);
            }
        }
        catch (http_exception const& e)
        {
            // wcout << e.what() << endl;
        }
            })
        .wait();
    request.reply(status_codes::OK, answer);
}

void handle_post(web::http::http_request request) {
    handle_request(
        request,
        [](web::json::value const& jvalue, json::value& answer) {
            answer = jvalue;
            try {
                lemmatization::lemmatizeJSON(answer);
            }
            catch (std::exception e) {
                answer = web::json::value::object();
                answer["error"] = web::json::value::string(e.what());
            }
            catch (...) {
                answer = web::json::value::object();
                answer["error"] = web::json::value::string("Unknown error occured");
            }
        });
}

int main() {
    http_listener listener("http://0.0.0.0:5000");
    listener.support(methods::POST, handle_post);

    try {
        listener
            .open()
            .then([&listener]() {})
            .wait();

        while (true);
    } catch (std::exception const& e) {
        std::wcout << e.what() << std::endl;
    }

    return 0;
}
