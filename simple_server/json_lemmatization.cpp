#include "json_lemmatization.hpp"
#include <exception>
namespace lemmatization {
    label::label(web::json::value json_label) {
        end = json_label.at("end").as_integer();
        endToken = json_label.at("endToken").as_integer();
        fieldName = json_label.at("fieldName").as_string();
        name = json_label.at("name").as_string();
        score = json_label.at("score").as_double();
        serviceName = json_label.at("serviceName").as_string();
        start = json_label.at("start").as_integer();
        startToken = json_label.at("startToken").as_integer();
        auto json_value = json_label.at("value");
        if (json_value.is_string()) value = json_value.as_string();
        else value = json_value.as_array()[0].as_string();
        std::transform(value.begin(), value.end(), value.begin(), ::tolower);
    }

    web::json::value label::jsonize() {
        web::json::value res;
        res["end"] = end;
        res["endToken"] = endToken;
        res["fieldName"] = web::json::value::string(fieldName);
        res["name"] = web::json::value::string(name);
        res["score"] = score;
        res["serviceName"] = web::json::value::string(serviceName);
        res["start"] = start;
        res["startToken"] = startToken;
        res["value"] = web::json::value::string(value);
        return res;
    }

    void doc::initialize(web::json::value json) {
        try {
            initLabels(json);
            initLemmas();
        }
        catch (...) {
            throw std::runtime_error("Could not parse json correctly");
        }
    }

    std::vector<label> doc::makeNewLabels(CascadeLemmatizer& lemmatizer) {
        std::vector<label> newLabels;
        for (auto label : labels) {
            if (label.serviceName == "NER") {
                std::stringstream base;
                std::stringstream ctag;
                std::string separator = "";
                for (int i = label.startToken; i <= label.endToken; i++) {
                    base << separator << lemmas[i].first;
                    ctag << separator << lemmas[i].second;
                    separator = " ";
                }
                auto ctag_str = ctag.str();
                auto base_str = base.str();
                auto new_val = lemmatizer.lemmatizeS(label.value, base_str, ctag_str, false);

                label.value = new_val;
                label.fieldName = "polem";
                label.name = "polem";
                label.serviceName = "Polem";

                newLabels.push_back(label);
            }
        }

        return newLabels;
    };

    void doc::initLabels(web::json::value json) {
        auto json_labels = json.at("labels").as_array();
        for (auto& json_label : json_labels) {
            labels.push_back(label(json_label));
        }
    }
    void doc::initLemmas() {
        for (auto label : labels) {
            if (label.name == "posTag") lemmas[label.startToken].second = label.value;
            else if (label.name == "lemmas") lemmas[label.startToken].first = label.value;
        }
    };

    void lemmatizeJSON(web::json::value& json) {
        CascadeLemmatizer lemmatizer = CascadeLemmatizer::assembleLemmatizer();
        auto& jsonDocs = json.at("docs").as_array();

        for (auto& jsonDoc : jsonDocs) {
            doc deserial_doc(jsonDoc);
            auto new_labels = deserial_doc.makeNewLabels(lemmatizer);
            std::vector<web::json::value> labels;
            for (auto& label : new_labels) {
                auto json_label = label.jsonize();
                labels.push_back(json_label);
            }
            for (auto label : jsonDoc.at("labels").as_array()) {
                labels.push_back(label);
            }

            auto new_json_labels = web::json::value().array();
            for (size_t i = 0; i < labels.size(); i++) {
                new_json_labels[i] = labels[i];
            }

            jsonDoc["labels"] = new_json_labels;
        }
    }
}

