FROM ubuntu:xenial
#### installing required packages
RUN apt-get update && apt-get -y upgrade
RUN apt-get -y install libboost-all-dev libicu-dev git-core wget cmake libantlr-dev libloki-dev python-dev swig libxml2-dev libsigc++-2.0-dev libglibmm-2.4-dev libxml++2.6-dev
#### downloading prerequisities...
RUN mkdir /build
WORKDIR /build
RUN git clone https://github.com/CLARIN-PL/Polem.git
RUN git clone http://nlp.pwr.wroc.pl/corpus2.git
RUN git clone http://nlp.pwr.edu.pl/wccl.git
RUN wget -O morfeusz2-2.0.0-Linux-amd64.deb https://nextcloud.clarin-pl.eu/index.php/s/VVIvx4w20azcWbp/download
RUN dpkg -i morfeusz2-2.0.0-Linux-amd64.deb
#### ... and building them
# corpus2
RUN cd corpus2
RUN mkdir bin
WORKDIR /build/corpus2/bin
RUN cmake ..
RUN make -j4
RUN make -j4
RUN make install
RUN ldconfig
# wccl
RUN mkdir /build/wccl/bin
WORKDIR /build/wccl/bin
RUN cmake ..
RUN make -j4
RUN make -j4
RUN make install
RUN ldconfig
# installing polem
RUN apt-get -y install default-jdk
RUN mkdir -p /build/Polem/build



RUN rm /build/Polem/build -rf
WORKDIR /build/Polem/build
RUN apt-get -y install libssl-dev
RUN apt -y purge --auto-remove cmake
RUN wget https://cmake.org/files/v3.19/cmake-3.19.1.tar.gz
RUN tar -xzvf cmake-3.19.1.tar.gz
WORKDIR /build/Polem/build/cmake-3.19.1
RUN ./bootstrap
RUN make -j4
RUN make install
RUN export PATH=/usr/local/bin/:$PATH
WORKDIR /build/Polem/build/

RUN apt-get -y install software-properties-common
RUN apt-get -y update
RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt-get -y update
RUN apt-get -y install python3.5

RUN cmake ..
RUN make -j4
RUN make install
RUN mkdir /data
RUN mv /build/Polem/corpus /data
WORKDIR /data
ENV LANG=pl_PL.utf-8

COPY . /build
WORKDIR /build

RUN git clone https://github.com/Microsoft/cpprestsdk.git casablanca
RUN mkdir /build/casablanca/build
WORKDIR /build/casablanca/build
RUN apt-get -y install g++ git libboost-atomic-dev libboost-thread-dev libboost-system-dev libboost-date-time-dev libboost-regex-dev libboost-filesystem-dev libboost-random-dev libboost-chrono-dev libboost-serialization-dev libwebsocketpp-dev openssl libssl-dev ninja-build
RUN cmake -G Ninja ..
RUN ninja
RUN ninja install

RUN mkdir /build/build
WORKDIR /build/build
RUN cmake ..
RUN make
EXPOSE 5000
CMD ["/build/build/simple_server/simple_server"]